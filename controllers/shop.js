const Product = require('../models/product');

exports.getProducts = (req, res, next) => {
  Product.findAll()
    .then((products) => {
      res.render('shop/product-list', {
        pageTitle: 'Products list',
        products,
        path: '/products',
      });
    })
    .catch((error) => console.log(error));
};

exports.getProductDetails = (req, res, next) => {
  const id = req.params.id;
  Product.findByPk(id)
    .then((product) => {
      res.render('shop/product-details', {
        pageTitle: product.title,
        product,
        path: '/products',
      });
    })
    .catch((error) => console.log(error));
};

exports.getHomePage = (req, res, next) => {
  Product.findAll()
    .then((products) => {
      res.render('shop/home', {
        pageTitle: 'Home page',
        products,
        path: '/',
      });
    })
    .catch((error) => console.log(error));
};

exports.getCart = (req, res, next) => {
  req.user.getCart()
    .then((cart) => cart.getProducts())
    .then((products) => {
      res.render('shop/cart', {
        pageTitle: 'Cart page',
        path: '/cart',
        products,
      });
    })
    .catch((error) => console.log(error));
};

exports.postItemToCart = (req, res, next) => {
  const productId = req.body.productId;
  let fetchedCart;
  let newQuantity = 1;
  req.user.getCart()
    .then((cart) => {
      fetchedCart = cart;
      return cart.getProducts({ where: { id: productId } });
    })
    .then((products) => {
      let product;
      if (products.length > 0) {
        product = products[0];
      }
      if (product) {
        const oldQuantity = product.cartItem.quantity;
        newQuantity = oldQuantity + 1;
        return product;
      }
      return Product.findByPk(productId);
    })
    .then((product) => fetchedCart.addProduct(product, { through: { quantity: newQuantity } }) )
    .then(() => res.redirect('/cart'))
    .catch((error) => console.log(error));
};

exports.postDeleteCartItem = (req, res, next) => {
  const prodId = req.body.productId;
  req.user.getCart()
    .then((cart) => cart.getProducts({ where: { id: prodId } }))
    .then(([product]) => product.cartItem.destroy())
    .then(() => res.redirect('/cart'))
    .catch((error) => console.log(error));
};

exports.getOrders = (req, res, next) => {
  req.user.getOrders({ include: ['products'] })
    .then((orders) => {
      res.render('shop/orders', {
        pageTitle: 'Orders page',
        path: '/orders',
        orders,
      });
    })
    .catch((error) => console.log(error));
};

exports.postOrder = (req, res, next) => {
  let products;
  let fetchedCart;
  req.user.getCart()
    .then((cart) => {
      fetchedCart = cart;
      return cart.getProducts();
    })
    .then((p) => {
      products = p;
      return req.user.createOrder();
    })
    .then((order) => {
      return order.addProducts(products.map((p) => {
        p.orderItem = {
          quantity: p.cartItem.quantity,
        };
        return p;
      }
      ));
    })
    .then((data) => fetchedCart.setProducts(null))
    .then(() => res.redirect('/orders'))
    .catch((error) => console.log(error));
};

exports.getOrderDetails = (req, res, next) => {
  const orderId = req.params.id;

  req.user.getOrders({
    where: { id: orderId },
    include: ['products'],
  })
    .then(([order]) => {
      res.render('shop/order-details', {
        pageTitle: `Order page #${order.id}`,
        path: '/orders',
        order,
      });
    })
    .catch((error) => console.log(error));
};

