const Product = require("../models/product");

exports.getAddProduct = (req, res, next) => {
  res.render("admin/edit-product", {
    pageTitle: "Add product",
    path: "/admin/add-product",
    editing: false,
  });
};

exports.postEditProduct = (req, res, next) => {
  const { productId: id, title, price, imageUrl, description } = req.body;
  Product.findByPk(id)
    .then((product) => {
      product.price = price;
      product.description = description;
      product.imageUrl = imageUrl;
      product.title = title;
      return product.save();
    })
    .then(() => res.redirect("/admin/products"))
    .catch((error) => console.log(error));
};

exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;
  if (!editMode) return res.redirect('/');
  const productId = req.params.id;
  // By Sequelize relations
  req.user.getProducts({ where: { id: productId } })
    .then(([product]) => {
      if (!product) return res.redirect('/');
      res.render("admin/edit-product", {
        pageTitle: "Edit product",
        path: "/admin/edit-product",
        editing: editMode,
        product,
      });
    });
  
  // By Sequelize model
  // Product.findAll({ where: { id: productId } })
  // Product.findByPk(productId).then((product) => {
  //   if (!product) return res.redirect('/');
  //   res.render("admin/edit-product", {
  //     pageTitle: "Edit product",
  //     path: "/admin/edit-product",
  //     editing: editMode,
  //     product,
  //   });
  // });
};

exports.getPostProduct = (req, res, next) => {
  const { title, price, imageUrl, description } = req.body;
  req.user.createProduct({
    title,
    price,
    imageUrl,
    description,
    userId: req.user.id,
  })
    .then(() => res.redirect("/"))
    .catch((error) => {
      console.log(error);
    });
};

exports.deleteAdminProduct = (req, res, next) => {
  const { productId } = req.body;
  Product.findByPk(productId)
    .then((product) => product.destroy())
    .then(() => res.redirect("/admin/products"))
    .catch((error) => console.log(error));
};

exports.getAdminProducts = (req, res, next) => {
  req.user.getProducts()
  // Product.findAll()
    .then((products) => {
      res.render("admin/products", {
        pageTitle: "Admin products",
        products,
        path: "/admin/products",
      });
    })
    .catch((error) => console.log(error));
};
