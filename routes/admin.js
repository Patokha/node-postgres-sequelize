const express = require('express');

const router = express.Router();

const { getAddProduct, getPostProduct, getAdminProducts, getEditProduct, postEditProduct, deleteAdminProduct } = require('../controllers/admin');

router.get('/add-product', getAddProduct);

router.post('/add-product', getPostProduct);

router.get('/products', getAdminProducts);

router.get('/edit-product/:id', getEditProduct);

router.post('/edit-product', postEditProduct);

router.post('/delete-product', deleteAdminProduct);
module.exports = router;
