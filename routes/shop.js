const express = require("express");

const router = express.Router();

const {
  getProducts,
  getHomePage,
  getCart,
  getOrders,
  getProductDetails,
  postItemToCart,
  postDeleteCartItem,
  postOrder,
  getOrderDetails,
} = require("../controllers/shop");

router.get("/", getHomePage);

router.get("/products", getProducts);

router.get("/product/:id", getProductDetails);

router.get("/cart", getCart);

router.post("/cart-delete-item", postDeleteCartItem);

router.post("/cart", postItemToCart);

router.get("/orders", getOrders);

router.post("/create-order", postOrder);

router.get("/order/:id", getOrderDetails);
module.exports = router;
