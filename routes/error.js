const express = require('express');
const router = express.Router();

const { getErrorPage } = require('../controllers/error');

router.use(getErrorPage);

module.exports = router;
