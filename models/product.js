const Sequelize = require('sequelize');
const sequelize = require('../helpers/database');


const Product = sequelize.define('product', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
    len: {
      args: [0, 30],
    },
  },
  description: {
    type: Sequelize.STRING,
    len: {
      args: [0, 250],
    },
  },
  imageUrl: {
    allowNull: false,
    type: Sequelize.STRING,
    len: {
      args: [0, 150],
    },
  },
  price: {
    allowNull: false,
    type: Sequelize.DOUBLE,
  },
});

module.exports = Product;
