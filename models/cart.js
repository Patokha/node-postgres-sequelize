const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Cart = sequelize.define('cart', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER,
  },
});

module.exports = Cart;
