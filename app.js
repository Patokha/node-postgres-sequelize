const express = require('express');
const app = express();

const path = require('path');

app.set('view engine', 'ejs');
app.set('views', 'views');

const sequelize = require('./helpers/database');

const Product = require('./models/product');
const User = require('./models/user');
const Cart = require('./models/cart');
const CartItem = require('./models/cart-item');
const Order = require('./models/order');
const OrderItem = require('./models/order-item');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const errorRoute = require('./routes/error');

const PORT = 3333;
app.use(express.urlencoded({ extended: true })); 
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
  User.findByPk(1)
    .then((user) => {
      req.user = user; 
      next();
    })
    .catch((error) => console.log(error));
});
app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorRoute);

Product.belongsTo(User, { constraints: true, onDelete: 'CASCADE' });
User.hasMany(Product);
User.hasOne(Cart);
Cart.belongsTo(User);
Cart.belongsToMany(Product, { through: CartItem }); // one cart can have many products, defining through what association it should be related
Product.belongsToMany(Cart, { through: CartItem }); // one product can be in multiple carts

Order.belongsTo(User);
User.hasMany(Order);
Order.belongsToMany(Product, { through: OrderItem });

let userHardcode;
sequelize
  // .sync({ force: true })
  .sync()
  .then((result) => {
    console.log("Connected to postgres...");
    return User.findByPk(1);
  })
  .then((user) => {
    if (!user) return User.create({ name: 'Sergii', email: 'patokha91@gmail.com' });
    userHardcode = user;
    return user;
  })
  .then((user) => user.getCart({ where: { id: 1 } }))
  .then((cart) => {
    if (!cart) return userHardcode.createCart();
    return cart;
  })
  .then(() => app.listen(PORT, () => console.log(`Server is running at ${PORT}`)))
  .catch((err) => console.log(err));
