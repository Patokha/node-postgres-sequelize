const Sequelize = require('sequelize');

const sequelize = new Sequelize('e-shop', process.env.user_name, process.env.user_password, {
  host: 'localhost',
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },
});

module.exports = sequelize;
